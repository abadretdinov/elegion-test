package com.histler.appbase.dao;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Badr on 28.05.2016.
 */
public interface CreateTableDao {
    void onCreate(SQLiteDatabase database);

    void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion);
}
