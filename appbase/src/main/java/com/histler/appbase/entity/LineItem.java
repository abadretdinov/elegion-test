package com.histler.appbase.entity;

import java.io.Serializable;

/**
 * Created by Badr
 * on 13.08.2016 19:38.
 */
public class LineItem<T> implements Serializable {
    public T data;
    public boolean isHeader;

    public LineItem(T data, boolean isHeader) {
        this.data = data;
        this.isHeader = isHeader;
    }
}
