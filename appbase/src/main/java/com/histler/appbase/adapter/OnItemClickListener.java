package com.histler.appbase.adapter;

import android.view.View;

/**
 * Created by Badr
 * 12.12.2014
 * 16:48
 */
public interface OnItemClickListener {
    void onItemClick(View view, int position);
}
