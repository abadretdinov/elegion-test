package com.histler.weather;

import android.app.Application;

/**
 * Created by Badr
 * on 13.08.2016 20:38.
 */
public class WeatherApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Initializer.initialize();
    }

}
