package com.histler.weather.remote.api.openweather;

/**
 * Created by Badr
 * on 09.07.2016 14:39.
 */
public interface Constants {
    String ICON_TEMPLATE_URL = "http://openweathermap.org/img/w/{0}.png";
}
