package com.histler.weather.remote.api.openweather.daily;

import java.io.Serializable;

/**
 * Created by Badr
 * on 09.07.2016 15:52.
 */
public class Clouds implements Serializable {
    private int all;

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }
}
